/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ci;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import hudson.Extension;
import hudson.model.AbstractProject;
import hudson.model.Cause.UserIdCause;
import hudson.model.Computer;
import hudson.model.Describable;
import hudson.model.Descriptor;
import hudson.model.Executor;
import hudson.model.Queue;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.model.listeners.RunListener;
import jenkins.model.Jenkins;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.StaplerRequest;

/**
 * The RunListener implementation which performs all the necessary ci.middleware.vt.edu maintenance tasks.
 *
 * @author ememisya
 */
@Extension
public class Maintenance extends RunListener<Run<?, ?>> implements Describable<Maintenance>
{

  /**
   *
   * Logger instance for this plugin.
   *
   */
  private static final Logger LOGGER = Logger.getLogger("jenkins.UpdateMavenAfterQueue");
  /**
   *
   * Wait 10 seconds initially to ensure the busy time is reached.
   *
   */
  private static final long INITIAL_WAIT_PERIOD = 20000;
  /**
   *
   * Wait 5 seconds per queue item just to make sure they will also be waited on once they reach the executor phase.
   *
   */
  private static final long WAIT_PERIOD_PER_QUEUE_ITEM = 5000;
  /**
   *
   * Don't ever wait more than 3 minute increments. With 0.1 (10%) decay period, maximum wait time comes around to be
   * 30~ minutes.
   *
   */
  private static final long MAXIMUM_WAIT_PERIOD = 60000 * 3;
  /**
   *
   * Additional padding for executor wait time estimation.
   *
   */
  private static final long ADDITIONAL_EXECUTOR_WAIT_TIME = 5000;
  /**
   *
   * Subtract 10% of the wait time from itself to reach closer to final script execution.
   *
   */
  private static final double DECAY_WAIT_PERIOD = 0.1;
  /**
   *
   * Don't go below 50ms minimum in checking for executor states.
   *
   */
  private static final long MINIMUM_DECAY_PERIOD = 50;

  /**
   * Default constructor
   */
  public Maintenance()
  {
  }

  /**
   * Starts the listening process if the cause is an instance of UserIdCause.
   *
   * @param runnable Run object of this job instance.
   * @param listener Task listener for the build process.
   */
  @Override
  public void onStarted(final Run runnable, final TaskListener listener)
  {
    super.onStarted(runnable, listener);
    if (getDescriptorImpl().isRunUpdateMavenScript()) {
      String startedByUser = null;
      for (Object cause : runnable.getCauses()) {
        if (cause instanceof UserIdCause) {
          final UserIdCause user = (UserIdCause) cause;
          startedByUser = user.getUserName();
          final StringBuilder logMsg = new StringBuilder();
          logMsg.append("Project: \"");
          logMsg.append(runnable.getParent().getDisplayName());
          logMsg.append("\" ");
          logMsg.append(user.getShortDescription());
          LOGGER.info(logMsg.toString());
          break;
        }
      }
      if (startedByUser != null) {
        if (!WorkerThread.QUEUE_LIST.contains(startedByUser)) {
          WorkerThread.QUEUE_LIST.add(runnable.getExternalizableId() +
                  startedByUser);
        }
      }
    }
  }

  /**
   * When the project build finalizes the plugin begins checking for quiet time by listening on the activity of the
   * executors and items on queue. It uses the singleton WorkerThread to keep track of the listening process.
   *
   * @param runnable Run object of this job instance.
   */
  @Override
  public void onFinalized(final Run runnable)
  {
    if (getDescriptorImpl().isRunUpdateMavenScript()) {
      if (!WorkerThread.QUEUE_LIST.isEmpty()) {
        int removeId = -1;
        for (int i = 0; i < WorkerThread.QUEUE_LIST.size(); i++) {
          final String queue = WorkerThread.QUEUE_LIST.get(i);
          if (queue.startsWith(runnable.getExternalizableId())) {
            removeId = i;
            break;
          }
        }
        if (removeId != -1) {
          final String queue = WorkerThread.QUEUE_LIST.get(removeId);
          WorkerThread.QUEUE_LIST.remove(removeId);
          final String userName = queue.substring(
                  runnable.getExternalizableId().length());
          WorkerThread.WORKER.schedule(new Runnable()
          {
            @Override
            public void run()
            {
              waitUntilQuietAndRunMavenRepoScript(INITIAL_WAIT_PERIOD, userName);
            }
          }, INITIAL_WAIT_PERIOD, TimeUnit.MILLISECONDS);
        }
      }
    } else {
      if (!WorkerThread.QUEUE_LIST.isEmpty()) {
        WorkerThread.QUEUE_LIST.clear();
      }
    }
  }

  /**
   * This method looks through all available Computer instances and goes through their executors for activity.
   *
   * @return The total estimated activity time of all executors along with any configured additional delays for items on
   * queue.
   */
  private long getEstimatedWaitTimeForAllTasks()
  {
    long estimatedWaitTime = 0;
    final Jenkins jenkinsInstance = Jenkins.getInstance();
    if (jenkinsInstance != null &&
            jenkinsInstance.getComputers() != null) {
      for (Computer computer : jenkinsInstance.getComputers()) {
        final List<Executor> executors = computer.getExecutors();
        if (executors != null) {
          for (Executor executor : executors) {
            if (executor.getEstimatedRemainingTimeMillis() > 0) {
              estimatedWaitTime
                      += executor.getEstimatedRemainingTimeMillis() +
                      ADDITIONAL_EXECUTOR_WAIT_TIME;
            }
          }
        }
      }
      final Queue.Item[] pendingItems = jenkinsInstance.getQueue().getItems();
      if (pendingItems != null && pendingItems.length > 0) {
        estimatedWaitTime += pendingItems.length * WAIT_PERIOD_PER_QUEUE_ITEM;
      }
    }
    return estimatedWaitTime;
  }

  /**
   * A recursive method to listen for executor activity to determine when activity dies down so that the maven
   * repository update script may be executed. When the wait time represented in miliseconds reaches zero the script is
   * fired.
   *
   * @param waitPeriod The wait period which will slowly decay to zero.
   * @param userName The username for which the script will be ran as.
   *
   */
  private void waitUntilQuietAndRunMavenRepoScript(
          final long waitPeriod,
          final String userName)
  {
    final long waitForMiliseconds;
    if (waitPeriod > MAXIMUM_WAIT_PERIOD) {
      waitForMiliseconds = MAXIMUM_WAIT_PERIOD;
    } else {
      waitForMiliseconds = getEstimatedWaitTimeForAllTasks() +
              (waitPeriod < 0 ? 0 : waitPeriod);
    }
    if (waitForMiliseconds > 0) {
      final long decayByRatio
              = (new Double(
                      Math.ceil(waitForMiliseconds * DECAY_WAIT_PERIOD))).longValue();
      final long decayTime;
      if (decayByRatio < MINIMUM_DECAY_PERIOD) {
        decayTime = MINIMUM_DECAY_PERIOD;
      } else {
        decayTime = decayByRatio;
      }
      WorkerThread.WORKER.schedule(new Runnable()
      {
        @Override
        public void run()
        {
          waitUntilQuietAndRunMavenRepoScript(
                  waitForMiliseconds - decayTime,
                  userName);
        }
      }, waitForMiliseconds, TimeUnit.MILLISECONDS);
    } else {
      final ProcessBuilder probuilder = new ProcessBuilder(
              getDescriptorImpl().getUpdateMavenRepositoryScriptPath(),
              getDescriptorImpl().getMavenRepositoryPath(),
              getDescriptorImpl().getMavenBranch(),
              userName);
      try {
        probuilder.start();
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Ran script for ");
        stringBuilder.append(userName);
        LOGGER.info(stringBuilder.toString());
      } catch (IOException ex) {
        LOGGER.log(Level.SEVERE, "Could not run maven update script.", ex);
      }
    }
  }

  @Override
  public Descriptor<Maintenance> getDescriptor()
  {
    return getDescriptorImpl();
  }

  /**
   * Descriptor instance for this plugin.
   * @return DescriptorImpl
   */
  public DescriptorImpl getDescriptorImpl()
  {
    return (DescriptorImpl) Jenkins.getInstance().getDescriptorOrDie(Maintenance.class);
  }

  /**
   * The Descriptor implementation for this class.
   */
  @Extension
  public static final class DescriptorImpl extends Descriptor<Maintenance>
  {

    /**
     * Whether to run the update maven repository script after each manual build.
     */
    private boolean runUpdateMavenScript = true;

    /**
     * Path to the Maven Repository Update Script (ex: /apps/local/bin/update-maven-repo.sh).
     */
    private String updateMavenRepositoryScriptPath = "/apps/local/bin/update-maven-repo.sh";

    /**
     * Path to the Maven Repository (ex: /apps/data/mw-maven).
     */
    private String mavenRepositoryPath = "/apps/data/mw-maven";

    /**
     * Branch name to auto commit to (ex: master).
     */
    private String mavenBranch = "master";

    /**
     * Default constructor.
     */
    public DescriptorImpl()
    {
      load();
    }

    /**
     * This human readable name is used in the configuration screen.
     *
     * @return Name of this plugin.
     */
    @Override
    public String getDisplayName()
    {
      return "Maintain Middleware-CI";
    }

    /**
     * Whether or not this plugin applies to individual projects as a build step.
     *
     * @param projectClass project class to check for.
     * @return boolean
     */
    public boolean isApplicable(final Class<? extends AbstractProject> projectClass)
    {
      // Can only be configured globally.
      return false;
    }

    @Override
    public boolean configure(final StaplerRequest request, final JSONObject formData) throws FormException
    {
      setRunUpdateMavenScript(formData.getBoolean("runUpdateMavenScript"));
      setUpdateMavenRepositoryScriptPath(formData.getString("updateMavenRepositoryScriptPath"));
      setMavenRepositoryPath(formData.getString("mavenRepositoryPath"));
      setMavenBranch(formData.getString("mavenBranch"));
      save();
      return super.configure(request, formData);
    }

    /**
     * Getter for runUpdateMavenScript
     * @return boolean
     */
    public boolean isRunUpdateMavenScript()
    {
      return runUpdateMavenScript;
    }

    /**
     * Setter for runUpdateMavenScript
     * @param runUpdateMavenScriptParameter runUpdateMavenScript
     */
    public void setRunUpdateMavenScript(final boolean runUpdateMavenScriptParameter)
    {
      runUpdateMavenScript = runUpdateMavenScriptParameter;
    }

    /**
     * Getter for updateMavenRepositoryScriptPath
     * @return String
     */
    public String getUpdateMavenRepositoryScriptPath()
    {
      return updateMavenRepositoryScriptPath;
    }

    /**
     * Setter for updateMavenRepositoryScriptPath
     * @param updateMavenRepositoryScriptPathParameter updateMavenRepositoryScriptPath
     */
    public void setUpdateMavenRepositoryScriptPath(final String updateMavenRepositoryScriptPathParameter)
    {
      updateMavenRepositoryScriptPath = updateMavenRepositoryScriptPathParameter;
    }

    /**
     * Getter for mavenRepositoryPath
     * @return String
     */
    public String getMavenRepositoryPath()
    {
      return mavenRepositoryPath;
    }

    /**
     * Setter for mavenRepositoryPath
     * @param mavenRepositoryPathParameter mavenRepositoryPath
     */
    public void setMavenRepositoryPath(final String mavenRepositoryPathParameter)
    {
      mavenRepositoryPath = mavenRepositoryPathParameter;
    }

    /**
     * Getter for mavenBranch
     * @return String
     */
    public String getMavenBranch()
    {
      return mavenBranch;
    }

    /**
     * Setter for mavenBranch
     * @param mavenBranchParameter mavenBranch
     */
    public void setMavenBranch(final String mavenBranchParameter)
    {
      mavenBranch = mavenBranchParameter;
    }
  }

}
