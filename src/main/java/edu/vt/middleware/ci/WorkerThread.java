/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ci;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Singleton which provides a scheduled executor service and a queue list for concurrent project build execution.
 *
 * @author ememisya
 */
public final class WorkerThread
{

  /**
   *
   * A WORKER instance for ScheduledExecutorService for listening instances to run on.
   *
   */
  public static final ScheduledExecutorService WORKER;

  /**
   *
   * The queue list which contains the username and the uniqueId of the job which first executed. This list keeps track
   * of activity listening requests each entry is cleared upon finalization of the job and listening begins.
   *
   */
  public static final List<String> QUEUE_LIST;

  static {
    QUEUE_LIST = Collections.synchronizedList(new ArrayList<String>());
    WORKER = Executors.newSingleThreadScheduledExecutor();
  }

  /**
   *
   * Private constructor so the singleton cannot be instantiated.
   *
   */
  private WorkerThread()
  {
  }
}
