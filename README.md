# Middleware CI Maintenance Plugin

* Version 0.1
  * Everytime a project is triggered to build via the web interface, this plugin will wait until all builds are complete, and trigger
  the script file defined in the system configuration.  Middleware uses this plugin for updating the maven repository by the triggering user.
    * It is important to note that this version doesnot support build parameters (i.e. for encrypted keys).
        * If you'd like for an ssh script to be triggered with your credentials after triggering a project build you must decrypt your key under ~/.ssh/gitlab_id_rsa

